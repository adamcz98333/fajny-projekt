# coding=utf-8
from datetime import datetime
from xlwings import Book
import numpy as np
import matplotlib.pyplot as plt

def clear_data():
    sheet_name0 = "Klient"
    sheet_name = "Zakupy"
    output = "ZakupyC"
    wb = Book.caller()
    try:
        wb.sheets[output].delete()
    except:
        pass
    wb.sheets.add(output, after=sheet_name0)
    sheet = wb.sheets.active
    table = wb.sheets[sheet_name].range('A1').current_region
    [customers, dates_raw, basket_sizes, basket_costs] = table.options(transpose=True).value
    customers = customers[1:]
    dates_raw = dates_raw[1:]
    basket_sizes = basket_sizes[1:]
    basket_costs = basket_costs[1:]

    #Daty ze stringów na typ danych
    dates = [datetime.strptime(data, '%d.%m.%Y') for data in dates_raw]
    # wektor do Excela - lista unikalnych klientów
    unique_customers = sorted(set(customers))
    # słownik - suma produktów na głowę klienta każdego
    sumSize_per_c = {unique_customer : 0 for unique_customer in unique_customers}
    # słownik - ilość zakupów na głowę klienta każdego
    numOfShopp_per_c = {unique_customer: 0 for unique_customer in unique_customers}
    # słownik - sumaryczny koszt zakupów na głowię klienta każdego
    sumCost_per_c = {unique_customer : 0.0 for unique_customer in unique_customers}
    # wypełnianie ww. słowników
    for customer, basket in zip(customers, basket_sizes):
        sumSize_per_c[customer] += basket
        numOfShopp_per_c[customer] += 1
    for customer, cost_ofBasket in zip(customers, basket_costs):
        sumCost_per_c[customer] += cost_ofBasket
    # wektor do Excela - średnia ilość zakupów na głowę
    avg_products = []
    for size, num in zip(sumSize_per_c.values(), numOfShopp_per_c.values()):
        avg_products.append(size/num)
    # wektor do Excela - średnia cana zakupów na głowę
    avg_costs = []
    for cost, num in zip(sumCost_per_c.values(), numOfShopp_per_c.values()):
        avg_costs.append(cost/num)
    # lista klientów i ich wszystkich dat posortowana jako krotki
    dates_per_customer = sorted([(customer, data) for customer, data in zip(customers, dates)])
    # słownik - ostatnia data zakupów na klienta
    last_date_per_c = {unique_customer : None for unique_customer in unique_customers}
    # wypełnianie słownika klient-data
    # (te będą ostatnie, bo się nadpisują z posortowanej listy wcześniej)
    for date in dates_per_customer:
        last_date_per_c[date[0]] = date[1]
    today = datetime.today()
    # wektor do Excela - ostatnie zakupy
    last_shop = []
    # wypełnianie ww.
    for date in last_date_per_c.values():
        last_shop.append(str((today - date).days)+" dni i "+\
                         str((today - date).seconds//3600)+" godzin")
    # słownik - pierwsza data zakupów na klienta
    first_date_per_c = {unique_customer : None for unique_customer in unique_customers}
    for date in dates_per_customer:
        if first_date_per_c[date[0]] is None:
            first_date_per_c[date[0]] = date[1]
        elif date[1] < first_date_per_c[date[0]]:
            first_date_per_c[date[0]] = date[1]
        else:
            pass
    # sumaryczny czas między zakupami na klienta
    sumTime_between_per_c = {unique_c: \
        (last_date_per_c[unique_c]-first_date_per_c[unique_c]).days for unique_c in unique_customers}
    print(sumTime_between_per_c)
    # wektor do Excela - średni czas między zakupami, 0 dla jednorazowych
    avg_between = []
    for uc in unique_customers:
        avg_between.append(sumTime_between_per_c[uc]/numOfShopp_per_c[uc])
    avg_products = [round(x,2) for x in avg_products]
    avg_costs = [round(x,2) for x in avg_costs]
    avg_products = [round(x,2) for x in avg_products]
    avg_products = [round(x,2) for x in avg_products]
    avg_between = [round(x,1) for x in avg_between]
    sheet.range('A1:E1').value = ["Customer ID", "Average Number of Products", "Average Cost [USD]", "Average Time Between Shoppings [days]", "Last Shopping from today"]
    sheet.range('A1:E1').color = (150, 150, 150, 150, 150)
    sheet.range('A2').options(transpose=True).value = unique_customers
    sheet.range('B2').options(transpose=True).value = avg_products
    sheet.range('C2').options(transpose=True).value = avg_costs
    sheet.range('D2').options(transpose=True).value = avg_between
    sheet.range('E2').options(transpose=True).value = last_shop
    sheet.range('A1').current_region.autofit()

    if __name__ == '__main__':
        xlwings.serve()
