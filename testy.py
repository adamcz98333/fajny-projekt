# coding=utf-8
from datetime import datetime, date
import time
# from xlwings import Book
import numpy as np
import matplotlib.pyplot as plt

def clear_data():
    customers = ["Customer1", "Customer2", "Customer2", "Customer3", "Customer1", "Customer2"]
    dates_raw = ["1.08.2015", "9.09.2014", "6.04.2016", "18.10.2014", "18.04.2014", "16.10.2015"]
    basket_sizes = [13, 15, 9, 3, 16, 2]
    basket_costs = [135.2, 624, 72.63, 43.8, 300, 20.68]
    #Daty ze stringów na typ danych
    dates = [datetime.strptime(data, '%d.%m.%Y') for data in dates_raw]
    # wektor do Excela - lista unikalnych klientów
    unique_customers = sorted(set(customers))
    # słownik - suma produktów na głowę klienta każdego
    sumSize_per_c = {unique_customer : 0 for unique_customer in unique_customers}
    # słownik - ilość zakupów na głowę klienta każdego
    numOfShopp_per_c = {unique_customer: 0 for unique_customer in unique_customers}
    # słownik - sumaryczny koszt zakupów na głowię klienta każdego
    sumCost_per_c = {unique_customer : 0.0 for unique_customer in unique_customers}
    # wypełnianie ww. słowników
    for customer, basket in zip(customers, basket_sizes):
        sumSize_per_c[customer] += basket
        numOfShopp_per_c[customer] += 1
    for customer, cost_ofBasket in zip(customers, basket_costs):
        sumCost_per_c[customer] += cost_ofBasket
    # wektor do Excela - średnia ilość zakupów na głowę
    avg_products = []
    for size, num in zip(sumSize_per_c.values(), numOfShopp_per_c.values()):
        avg_products.append(size/num)
    # wektor do Excela - średnia cana zakupów na głowę
    avg_costs = []
    for cost, num in zip(sumCost_per_c.values(), numOfShopp_per_c.values()):
        avg_costs.append(cost/num)
    # lista klientów i ich wszystkich dat posortowana jako krotki
    dates_per_customer = sorted([(customer, data) for customer, data in zip(customers, dates)])
    # słownik - ostatnia data zakupów na klienta
    last_date_per_c = {unique_customer : None for unique_customer in unique_customers}
    # wypełnianie słownika klient-data
    # (te będą ostatnie, bo się nadpisują z posortowanej listy wcześniej)
    for date in dates_per_customer:
        last_date_per_c[date[0]] = date[1]
    today = datetime.today()
    # wektor do Excela - ostatnie zakupy
    last_shop = []
    # wypełnianie ww.
    for date in last_date_per_c.values():
        last_shop.append(str((today - date).days)+" dni i "+\
                         str((today - date).seconds//3600)+" godzin")
    # słownik - pierwsza data zakupów na klienta
    first_date_per_c = {unique_customer : None for unique_customer in unique_customers}
    for date in dates_per_customer:
        if first_date_per_c[date[0]] is None:
            first_date_per_c[date[0]] = date[1]
        elif date[1] < first_date_per_c[date[0]]:
            first_date_per_c[date[0]] = date[1]
        else:
            pass
    # sumaryczny czas między zakupami na klienta
    sumTime_between_per_c = {unique_c: \
        (last_date_per_c[unique_c]-first_date_per_c[unique_c]).days for unique_c in unique_customers}
    # wektor do Excela - średni czas między zakupami, 0 dla jednorazowych
    avg_between = []
    for uc in unique_customers:
        avg_between.append(sumTime_between_per_c[uc]/numOfShopp_per_c[uc])

    print("unique_customers:")
    print(unique_customers)
    print("avg_products:")
    print(avg_products)
    print("avg_costs:")
    print(avg_costs)
    print("last_shop:")
    print(last_shop)
    print("avg_between:")
    print(avg_between)
if __name__ == '__main__':
    clear_data()
